/**
 * Test injectors
 */

/* eslint-disable import/no-unresolved */
import { memoryHistory } from 'react-router-dom';
import { Provider } from 'react-redux';
import { put } from 'redux-saga/effects';
import { shallow } from 'enzyme';
import React from 'react';
/* eslint-enable import/no-unresolved */

import configureStore from '../../configureStore';
import injectSaga, { ConsumedComponent } from '../injectSaga';
import * as sagaInjectors from '../sagaInjectors';

// Fixtures
const Component = () => null;

function* testSaga() {
  yield put({ type: 'TEST', payload: 'yup' });
}

describe('ConsumedComponent', () => {
  const props = {
    ComposedComponent: () => <div>Component</div>,
  };
  let output;

  beforeEach(() => {
    output = shallow(<ConsumedComponent {...props} />);
  });

  it('returns an empty span when sagaInjected is false', () => {
    expect(output.name()).toEqual('span');
    expect(output.find('ComposedComponent').exists()).toBeFalsy();
  });

  describe('props.sagaInjected is true', () => {
    beforeAll(() => {
      props.sagaInjected = true;
    });

    afterAll(() => {
      delete props.sagaInjected;
    });

    it('returns injected component', () => {
      expect(output.name()).not.toEqual('span');
      expect(output.find('ComposedComponent').exists()).toBeTruthy();
    });
  });
});

describe('injectSaga decorator', () => {
  let store;
  let injectors;
  let ComponentWithSaga;

  beforeAll(() => {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(script);
    sagaInjectors.default = jest.fn().mockImplementation(() => injectors);
  });

  beforeEach(() => {
    store = configureStore({}, memoryHistory);
    injectors = {
      injectSaga: jest.fn(),
      ejectSaga: jest.fn(),
    };
    ComponentWithSaga = injectSaga({
      key: 'test',
      saga: testSaga,
      mode: 'testMode',
    })(Component);
    sagaInjectors.default.mockClear();
  });

  it('should inject given saga, mode, and props', () => {
    const props = { test: 'test' };
    shallow(<ComponentWithSaga {...props} />, { context: { store } });

    expect(injectors.injectSaga).toHaveBeenCalledTimes(1);
    expect(injectors.injectSaga).toHaveBeenCalledWith(
      'test',
      { saga: testSaga, mode: 'testMode' },
      props,
    );
  });

  it('should eject on unmount with a correct saga key', () => {
    const props = { test: 'test' };
    const renderedComponent = shallow(<ComponentWithSaga {...props} />, {
      context: { store },
    });
    renderedComponent.unmount();

    expect(injectors.ejectSaga).toHaveBeenCalledTimes(1);
    expect(injectors.ejectSaga).toHaveBeenCalledWith('test');
  });

  it('should propagate props', () => {
    const props = { testProp: 'test' };
    const renderedComponent = shallow(
      <Provider store={store}>
        <ComponentWithSaga {...props} />
      </Provider>,
    );

    expect(renderedComponent.dive().prop('testProp')).toBe('test');
  });
});
