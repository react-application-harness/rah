/*
 * injectReducer
 */

/* eslint-disable import/no-unresolved */
import React from 'react';
import { ReactReduxContext } from 'react-redux';
/* eslint-enable import/no-unresolved */

import getInjectors from './reducerInjectors';

/**
 * Dynamically injects a reducer into the current, default redux context.
 * Adding context to the Provider store breaks the default context and this injector.
 *
 * const withReducer = withReducerInjector({ key: 'foo', reducer: fooReducer });
 * export default compose(
 *  withReducer,
 *  connect(
 *    mapStateToProps,
 *    mapDispatchToProps
 *  )
 * )(Component)
 * ...
 * <ComposedComponent />
 * ...
 *
 * @method
 * @param {string} key A key of the reducer
 * @param {function} reducer A reducer that will be injected
 *
 */
const withReducerInjector = ({ key, reducer }) => ComposedComponent => {
  class ReducerInjectorHOC extends React.Component {
    constructor(props) {
      super(props);
      this.storeRef = null;
    }

    componentDidMount() {
      const { injectReducer } = getInjectors(this.storeRef);
      injectReducer(key, reducer);
    }

    render() {
      return (
        <ReactReduxContext.Consumer>
          {({ store }) => {
            this.storeRef = store;
            return <ComposedComponent {...this.props} />;
          }}
        </ReactReduxContext.Consumer>
      );
    }
  }

  return ReducerInjectorHOC;
};

export default withReducerInjector;
