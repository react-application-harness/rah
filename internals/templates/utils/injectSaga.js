/*
 * injectSaga
 */

/* eslint-disable import/no-unresolved */
import React from 'react';
import PropTypes from 'prop-types';
import { ReactReduxContext } from 'react-redux';
/* eslint-enable import/no-unresolved */

import getInjectors from './sagaInjectors';

// Wrap component so child contents aren't rendered before the saga is connected
export const ConsumedComponent = props => {
  const { sagaInjected, ComposedComponent, ...rest } = props;

  if (!sagaInjected) {
    return <span />;
  }

  return <ComposedComponent {...rest} />;
};

ConsumedComponent.propTypes = {
  sagaInjected: PropTypes.bool,
  ComposedComponent: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func,
    PropTypes.element,
  ]).isRequired,
};

/**
 * Dynamically injects a saga, passes component's props as saga arguments
 *
 * @param {string} key A key of the saga
 * @param {function} saga A root saga that will be injected
 * @param {string} [mode] By default (constants.RESTART_ON_REMOUNT) the saga will be started on component mount and
 * cancelled with `task.cancel()` on component un-mount for improved performance. Another two options:
 *   - constants.DAEMON—starts the saga on component mount and never cancels it or starts again,
 *   - constants.ONCE_TILL_UNMOUNT—behaves like 'RESTART_ON_REMOUNT' but never runs it again.
 *
 */
const withSagaInjector = ({ key, saga, mode }) => ComposedComponent => {
  class SagaInjectorHOC extends React.Component {
    constructor(props) {
      super(props);
      this.storeRef = null;

      this.state = {
        injected: false,
      };
    }

    componentDidMount() {
      this.inject();
    }

    componentWillUnmount() {
      const { ejectSaga } = getInjectors(this.storeRef);
      ejectSaga(key);
    }

    inject = () => {
      if (!this.state.injected) {
        const { injectSaga } = getInjectors(this.storeRef);
        injectSaga(key, { saga, mode }, this.props);
        this.setState({ injected: true });
      }
    };

    render() {
      return (
        <ReactReduxContext.Consumer>
          {({ store }) => {
            this.storeRef = store;
            return (
              <ConsumedComponent
                {...this.props}
                sagaInjected={this.state.injected}
                ComposedComponent={ComposedComponent}
              />
            );
          }}
        </ReactReduxContext.Consumer>
      );
    }
  }

  return SagaInjectorHOC;
};

export default withSagaInjector;
