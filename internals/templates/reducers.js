/**
 * Combine all reducers in this file and export the combined reducers.
 */

/* eslint-disable import/no-unresolved */
import { combineReducers } from 'redux-immutable';
import { connectRouter } from 'connected-react-router/immutable';
import history from 'utils/history';

import languageProviderReducer from 'containers/LanguageProvider/reducer';
/* eslint-enable import/no-unresolved */

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer(injectedReducers = {}) {
  return combineReducers({
    router: connectRouter(history),
    language: languageProviderReducer,
    ...injectedReducers,
  });
}
