/**
 * Asynchronously loads the component for HomePage
 */
// eslint-disable-next-line import/no-unresolved
import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
