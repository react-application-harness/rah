import { fromJS } from 'immutable';

// eslint-disable-next-line import/no-unresolved
import { selectRouter, makeSelectLocation } from 'containers/App/selectors';

describe('selectRouter', () => {
  it('should select the router sate', () => {
    const routerState = fromJS({
      location: { path: 'test' },
    });
    const mockedState = fromJS({
      router: routerState,
    });
    expect(selectRouter(mockedState)).toEqual(routerState);
  });
});

describe('makeSelectLocation', () => {
  it('should select the location', () => {
    const router = fromJS({
      location: { pathname: '/foo' },
    });
    const mockedState = fromJS({
      router,
    });
    expect(makeSelectLocation()(mockedState)).toEqual(
      router.get('location').toJS(),
    );
  });
});
