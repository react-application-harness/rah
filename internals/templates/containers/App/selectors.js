import { createSelector } from 'reselect';

const selectRouter = state => state.get('router');

const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routeState => routeState.get('location').toJS(),
  );

export { selectRouter, makeSelectLocation };
