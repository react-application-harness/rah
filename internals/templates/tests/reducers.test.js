import createReducer from '../reducers';

describe('reducers', () => {
  it('should return correctly', () => {
    const reducer = createReducer()(undefined, {});
    expect(reducer.get('router')).toBeDefined();
  });
});
