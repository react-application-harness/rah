/**
 * @file A simple node script that validate's a progressive web app manifest
 * @param {string} --index The relative path from the app's `rah-config.js` to the app's `index.html` file after building. Typically this is `build/index.html`.
 * @example To run the validation script every time you build, add the following to your package.json
 * "scripts": {
 *  "postbuild": "node ./node_modules/rah/internals/scripts/validate-wam.js --index build/index.html",
 * }
 */

const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const htmlParser = require('htmlparser2');
const validateManifest = require('wam');

(function() {
  console.log('📲  Validating the progressive web app manifest...');

  const indexFile = process.argv.find(
    (val, index, array) => index > 0 && array[index - 1] === '--index',
  );
  const indexFilePath = indexFile && path.resolve(indexFile);

  checkIndexArgument()
    .then(readIndexToString)
    .then(writeIndexToParser)
    .then(resolveManifestFromHref)
    .then(checkForErrors)
    .then(onSuccessMessage)
    .catch(onValidationError);

  function checkIndexArgument() {
    const resolvePath = (resolve, reject) =>
      indexFilePath
        ? resolve(indexFilePath)
        : reject(
            new Error(
              'Please use a single argment "--index" pointing to your bundled index.html file.',
            ),
          );

    return new Promise(resolvePath);
  }

  function readIndexToString(fileName) {
    const readIndexFile = (resolve, reject) => {
      const readFileCb = (err, data) => (err ? reject(err) : resolve(data));
      fs.readFile(fileName, 'utf8', readFileCb);
    };

    return new Promise(readIndexFile);
  }

  function writeIndexToParser(htmlString) {
    const writeIndex = (resolve, reject) => {
      const onOpenTag = (name, { rel, href }) => {
        if (name === 'link' && rel === 'manifest') {
          resolve(href);
        }
      };
      const onCloseTag = (name, attribs) => {
        if (name === 'head') {
          reject(
            new Error(`No link element with a manifest found in ${indexFile}`),
          );
        }
      };
      const parser = new htmlParser.Parser(
        { onopentag: onOpenTag, onclosetag: onCloseTag },
        { decodeEntities: true },
      );
      parser.write(htmlString);
    };

    return new Promise(writeIndex);
  }

  function resolveManifestFromHref(href) {
    if (href === '') {
      return Promise.reject(
        new Error(
          `${indexFile} is not a valid manifest file. Please provide a URI instead of self-referencing the index file in your <link> element.`,
        ),
      );
    }
    if (!href) {
      return Promise.reject(
        new Error(
          `<link> to your manifest in ${indexFile} did not contain a valid href attribute.`,
        ),
      );
    }
    const pwaManifest = require(path.join(path.dirname(indexFilePath), href));
    return Promise.resolve(pwaManifest);
  }

  function checkForErrors(manifest) {
    const errors = validateManifest(manifest);
    if (errors.length === 0) {
      return Promise.resolve();
    } else if (errors.length >= 1) {
      return Promise.reject(errors);
    } else {
      return Promise.reject(
        new Error('Unexpected error validating web app manifest.'),
      );
    }
  }

  function onSuccessMessage() {
    process.stdout.write(chalk.green('✅  Passes!\n'));
  }

  function onValidationError(error) {
    process.stdout.write(chalk.red('❌  Encountered errors.\n'));
    if (error.message) {
      console.error(error);
    } else if (error.length) {
      error.forEach(err => {
        console.info(err);
      });
      console.log();
    }
  }
})();
