// Compute the packages in node_modules that need to be included in the transform
// eslint-disable-next-line import/no-unresolved
const targetPkg = require('../../../../package.json');
const rahPkg = require('../../package.json');
const targetInclude = (targetPkg.dllPlugin || {}).include || [];
const rahInclude = (rahPkg.dllPlugin || {}).include || [];
const esModules = rahInclude.concat(targetInclude).join('|');
module.exports = {
  rootDir: '../../../../',
  collectCoverageFrom: [
    'app/**/*.{js,jsx}',
    '!app/**/*.test.{js,jsx}',
    '!app/*/RbGenerated*/*.{js,jsx}',
    '!app/app.js',
    '!app/global-styles.js',
    '!app/*/*/Loadable.{js,jsx}',
  ],
  coverageThreshold: {
    global: {
      statements: 98,
      branches: 91,
      functions: 98,
      lines: 98,
    },
  },
  moduleDirectories: ['node_modules', 'app'],

  moduleNameMapper: {
    '.*\\.(css|less|styl|scss|sass)$':
      '<rootDir>/node_modules/rah/internals/mocks/cssModule.js',
    '.*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/node_modules/rah/internals/mocks/image.js',
  },
  setupFilesAfterEnv: ['./node_modules/rah/internals/testing/test-bundler.js'],
  setupFiles: [
    'raf/polyfill',
    './node_modules/rah/internals/testing/enzyme-setup.js',
  ],
  testRegex: 'tests/.*\\.test\\.js$',
  transform: {
    '^.+\\.js$': './node_modules/rah/internals/testing/jestPreprocess.js',
  },
  transformIgnorePatterns: [`./node_modules/(?!${esModules})`],
  snapshotSerializers: ['enzyme-to-json/serializer'],
};
