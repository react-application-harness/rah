const packageDefinition = require('../../package.json');
const babelOptions = packageDefinition.babel;
module.exports = require('babel-jest').createTransformer(babelOptions);
