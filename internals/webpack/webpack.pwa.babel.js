/**
 * @module A harness to generate a manifest using `webpack-pwa-manifest`. Merges default RAH settings with user-defined pwa manifest configs
 * @author Beverly Boydston
 */

const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const WebpackPwaManifest = require('webpack-pwa-manifest');

const defaults = require('../../rah-config.js');
const defaultPwaConfig = defaults.manifest;

/**
 * @function
 * @param {object} options - Webpack options
 * @returns {object} options - Webpack options with `webpack-pwa-manifest` pushed to plugins array
 */
module.exports = ({ plugins: incPlugins = [], ...options } = {}) => {
  let manifest = {};
  try {
    const appSettings = path.resolve(process.cwd(), 'rah-config.js');
    if (fs.existsSync(appSettings)) {
      // eslint-disable-next-line
      manifest = require(appSettings).manifest;
    }
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
  }

  return {
    ...options,
    plugins: [
      ...incPlugins, // Order is important, WebpackPwaManifest must appear after HtmlWebpackPlugin
      new WebpackPwaManifest(_.merge(defaultPwaConfig, manifest)),
    ],
  };
};
