// Important modules this config uses
const path = require('path');
const { compose } = require('redux');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const { HashedModuleIdsPlugin } = require('webpack');
const rahConfig = require('../../rah-config.js');
const pkg = require(path.resolve(process.cwd(), 'package.json'));
const { dllPlugin: { plugins: { exclude } = {} } = {} } = pkg;

const {
  indexPath,
  relativePaths = false,
  publicPath = '/',
  appShell,
} = rahConfig;

const plugins = [];
const excludePlugins = exclude || [];

if (!excludePlugins.includes('HtmlWebpackPlugin')) {
  plugins.push(
    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
      // Use target package index if there is one
      template: indexPath,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
      inject: true,
    }),
  );
}

if (!excludePlugins.includes('OfflinePlugin')) {
  plugins.push(
    // Put it in the end to capture all the HtmlWebpackPlugin's
    // assets manipulations and do leak its manipulations to HtmlWebpackPlugin
    new OfflinePlugin({
      relativePaths,
      publicPath,
      appShell: appShell || publicPath,
      autoUpdate: true,
      // No need to cache .htaccess. See http://mxs.is/googmp,
      // this is applied before any match in `caches` section
      excludes: ['.htaccess'],

      caches: {
        main: [':rest:'],

        // All chunks marked as `additional`, loaded after main section
        // and do not prevent SW to install. Change to `optional` if
        // do not want them to be preloaded at all (cached only when first loaded)
        additional: ['*.chunk.js'],
      },
      ServiceWorker: {
        events: true, // Emit events for when new service workers become available
      },
      // Removes warning for about `additional` section usage
      safeToUseOptionalCaches: true,
    }),
  );
}

if (!excludePlugins.includes('HashedModuleIdsPlugin')) {
  plugins.push(
    new HashedModuleIdsPlugin({
      hashFunction: 'sha256',
      hashDigest: 'hex',
      hashDigestLength: 20,
    }),
  );
}

const productionOptions = () => ({
  mode: 'production',

  // In production, we skip all hot-reloading stuff
  entry: [path.join(process.cwd(), 'app/app.js')],

  // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js',
  },

  optimization: {
    minimize: true,
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    splitChunks: { chunks: 'all' },
    runtimeChunk: true,
  },

  plugins,

  performance: {
    assetFilter: assetFilename =>
      !/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename),
  },
});

module.exports = compose(
  require('./webpack.base.babel'),
  require('./webpack.pwa.babel'),
  productionOptions,
);
