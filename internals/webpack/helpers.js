const _ = require('lodash');

const mergeSettings = (base = {}, target) => {
  let settings = Object.assign({}, base);

  if (target) {
    settings = _.mergeWith(settings, target, (objValue, srcValue) => {
      if (_.isArray(objValue)) {
        return _.uniq(objValue.concat(srcValue));
      }

      return undefined;
    });
  }

  return settings;
};

module.exports = { mergeSettings };
