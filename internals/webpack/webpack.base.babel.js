/**
 * COMMON WEBPACK CONFIGURATION
 */

const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const { mergeSettings } = require('./helpers');
const packageDefinition = require('../../package.json');
const targetPackage = require(`${process.cwd()}/package.json`);
const rahConfig = require('../../rah-config.js');
const {
  dllPlugin: {
    resolve: targetResolve = {},
    babel: targetBabel,
    include: includedTargetDeps = [],
  } = {},
} = targetPackage;

const { publicPath = '/' } = rahConfig;

const reactRouterDom = path.resolve(
  process.cwd(),
  'node_modules/react-router-dom',
);
const reactRouter = path.resolve(process.cwd(), 'node_modules/react-router');
const react = path.resolve(process.cwd(), 'node_modules/react');
const reactDom = path.resolve(process.cwd(), 'node_modules/react-dom');

// console.log('Target package def:', targetPackage)
// Remove this line once the following warning goes away (it was meant for webpack loader authors not users):
// 'DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic,
// see https://github.com/webpack/loader-utils/issues/56 parseQuery() will be replaced with getOptions()
// in the next major version of loader-utils.'
process.noDeprecation = true;

const includedDepsTests = includedTargetDeps.map(n => new RegExp(n));
const defaultBabel = packageDefinition.babel;
const defaultResolve = {};

// Specify to use target react-router-dom instance since it's a singleton and our
// packages need to reference the same instance
if (fs.existsSync(reactRouterDom)) {
  defaultResolve['react-router-dom'] = reactRouterDom;
}
if (fs.existsSync(reactRouter)) {
  defaultResolve['react-router'] = reactRouter;
}
if (fs.existsSync(react)) {
  defaultResolve.react = react;
}
if (fs.existsSync(reactDom)) {
  defaultResolve['react-dom'] = reactDom;
}

const babelConfig = mergeSettings(defaultBabel, targetBabel);

let COMMIT_HASH = false;
try {
  // eslint-disable-next-line global-require
  COMMIT_HASH = require('child_process')
    .execSync('git rev-parse HEAD')
    .toString()
    .trim();
} catch (err) {
  // eslint-disable-next-line no-console
  console.error('Probably not a git repo.', err);
}

const resolveAlias = Object.assign(defaultResolve, targetResolve.alias || {});
const moduleResolve = Object.assign(
  {
    modules: ['node_modules', 'app'].concat(includedTargetDeps),
    extensions: ['.js', '.jsx', '.json', '.react.js'],
    mainFields: ['browser', 'jsnext:main', 'main'],
    symlinks: false,
    fallback: {
      fs: false,
    },
  },
  targetResolve,
);

if (Object.keys(resolveAlias).length) {
  moduleResolve.alias = resolveAlias;
}

module.exports = options => ({
  mode: options.mode,
  entry: options.entry,
  output: Object.assign(
    {
      // Compile into js/build.js
      path: path.resolve(process.cwd(), 'build'),
      publicPath,
    },
    options.output,
  ), // Merge with env dependent settings
  optimization: options.optimization,
  module: {
    rules: [
      {
        test: /\.js$/, // Transform all .js files required somewhere with Babel
        include: [/app/].concat(includedDepsTests),
        use: {
          loader: 'babel-loader',
          options: babelConfig,
        },
      },
      {
        // Preprocess our own .css files
        // This is the place to add your own loaders (e.g. sass/less etc.)
        // for a list of loaders, see https://webpack.js.org/loaders/#styling
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
      {
        // Preprocess 3rd party .css files located in node_modules
        test: /\.css$/,
        include: [/node_modules/],
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2)$/,
        use: 'file-loader',
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              // Inline files smaller than 10 kB
              limit: 10 * 1024,
            },
          },
        ],
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              // Inline files smaller than 10 kB
              limit: 10 * 1024,
            },
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                enabled: false,
                // NOTE: mozjpeg is disabled as it causes errors in some Linux environments
                // Try enabling it in your environment by switching the config to:
                // enabled: true,
                // progressive: true,
              },
              gifsicle: {
                interlaced: false,
              },
              optipng: {
                optimizationLevel: 7,
              },
              pngquant: {
                quality: '65-90',
                speed: 4,
              },
            },
          },
        ],
      },
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.(mp4|webm)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
          },
        },
      },
    ],
  },
  plugins: options.plugins.concat([
    new webpack.ProvidePlugin({
      // make fetch available
      fetch: 'exports-loader?self.fetch!whatwg-fetch',
    }),

    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        APP_VERSION: JSON.stringify(targetPackage.version),
        APP_NAME: JSON.stringify(targetPackage.name),
        APP_AUTHOR: JSON.stringify(targetPackage.author),
        APP_REPO: JSON.stringify(
          targetPackage.repository && targetPackage.repository.url,
        ),
        COMMIT_HASH: COMMIT_HASH ? JSON.stringify(COMMIT_HASH) : undefined,
      },
    }),
  ]),
  resolve: moduleResolve,
  devtool: options.devtool,
  target: 'web', // Make web variables accessible to webpack, e.g. window
  performance: options.performance || {},
});
