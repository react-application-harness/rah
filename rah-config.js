/**
 * @module Default settings for rah-config. Create `rah-config.js` in your app's root directory to extend these settings. `rah-config.js` is a node configuration file not included in your app's build.
 * @exports Default settings for configuring a web app manifest
 */

const path = require('path');
const fs = require('fs');
const { mergeSettings } = require('./internals/webpack/helpers');
const targetRahPath = path.resolve(process.cwd(), 'rah-config.js');

module.exports = mergeSettings(
  {
    manifest: {
      name: 'React Application Harness',
      short_name: 'RAH',
      description:
        'Maintainable and installable react application development infrastructure.',
      background_color: '#fafafa',
      theme_color: '#000000',
      icons: [
        {
          src: path.resolve('node_modules/rah/assets/icon-512x512.png'),
          sizes: [72, 96, 120, 128, 144, 152, 167, 180, 192, 384, 512],
        },
      ],
    },
    indexPath: 'app/index.html',
  },
  // eslint-disable-next-line global-require
  fs.existsSync(targetRahPath) && require(targetRahPath),
);
